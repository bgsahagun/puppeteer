import puppeteer from "puppeteer";
import fs from "fs";
import config from "./config.json";

(async () => {
	const browser = await puppeteer.launch({ headless: false });
	const page = await browser.newPage();

	await page.goto("https://www.facebook.com/", { waitUntil: "networkidle0" });

	await page.type("#email", config.username, { delay: 30 });
	await page.type("#pass", config.password, { delay: 30 });

	await page.click("button[type='submit']");

	await page.waitForNavigation({ waitUntil: "networkidle0" });
	await page.waitForTimeout(15000);

	try {
		await page.waitForSelector('a[href="/me/"]');
		console.log("Successfully logged in");
	} catch (error) {
		console.log("Failed to login");
		process.exit(0);
	}

	let currentCookies = await page.cookies();

	fs.writeFileSync("./fbcookies.json", JSON.stringify(currentCookies));

	await browser.close();
})();
