const searchTerms = {
	fgen: ["fgen", "fph", "first philippine holdings"],
	lopez: ["lopezes", "lopez family"],
	coal: ["coal", "coal mura", "coal cheap"],
	chacha: ["charter change", "cha-cha"],
	natgas: ["natural gas", "nat gas"],
	// trillanes: ["trillanes"],
};

const fbFilterParameters = [
	"&filters=eyJycF9jcmVhdGlvbl90aW1lOjAiOiJ7XCJuYW1lXCI6XCJjcmVhdGlvbl90aW1lXCIsXCJhcmdzXCI6XCJ7XFxcInN0YXJ0X3llYXJcXFwiOlxcXCIyMDIxXFxcIixcXFwic3RhcnRfbW9udGhcXFwiOlxcXCIyMDIxLTFcXFwiLFxcXCJlbmRfeWVhclxcXCI6XFxcIjIwMjFcXFwiLFxcXCJlbmRfbW9udGhcXFwiOlxcXCIyMDIxLTEyXFxcIixcXFwic3RhcnRfZGF5XFxcIjpcXFwiMjAyMS0xLTFcXFwiLFxcXCJlbmRfZGF5XFxcIjpcXFwiMjAyMS0xMi0zMVxcXCJ9XCJ9In0%3D",
	"&filters=eyJycF9jcmVhdGlvbl90aW1lOjAiOiJ7XCJuYW1lXCI6XCJjcmVhdGlvbl90aW1lXCIsXCJhcmdzXCI6XCJ7XFxcInN0YXJ0X3llYXJcXFwiOlxcXCIyMDIxXFxcIixcXFwic3RhcnRfbW9udGhcXFwiOlxcXCIyMDIxLTFcXFwiLFxcXCJlbmRfeWVhclxcXCI6XFxcIjIwMjFcXFwiLFxcXCJlbmRfbW9udGhcXFwiOlxcXCIyMDIxLTEyXFxcIixcXFwic3RhcnRfZGF5XFxcIjpcXFwiMjAyMS0xLTFcXFwiLFxcXCJlbmRfZGF5XFxcIjpcXFwiMjAyMS0xMi0zMVxcXCJ9XCJ9IiwicnBfYXV0aG9yOjAiOiJ7XCJuYW1lXCI6XCJteV9ncm91cHNfYW5kX3BhZ2VzX3Bvc3RzXCIsXCJhcmdzXCI6XCJcIn0ifQ%3D%3D",
];

const dateRange = {
	start_date: new Date(),
	end_date: new Date(new Date() - 8 * 24 * 60 * 60 * 1000),
};

export { searchTerms, fbFilterParameters, dateRange };
