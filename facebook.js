import puppeteer from "puppeteer";
import { search } from "./utils.js";
import cookies from "./fbcookies.json";

const main = async () => {
	const browser = await puppeteer.launch();
	const page = await browser.newPage();
	await page.setUserAgent(
		"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.146 Safari/537.36"
	);

	await page.setCookie(...cookies);

	await search(page, "fb");

	console.log(`Finished searching`);

	await browser.close();
};

main();
