import puppeteer from "puppeteer";

(async () => {
	const browser = await puppeteer.launch({
		defaultViewport: null,
	});
	const page = await browser.newPage();

	await page.goto(
		"https://www.pse.com.ph/stockMarket/companyInfo.html?id=94&security=160&tab=0",
		{ waitUntil: "networkidle0" }
	);

	const companyName = await page.evaluate(
		() =>
			document.querySelector("#middleColumnCom #comTopInfoHead > h2")
				.textContent
	);

	const companySymbol = await page.evaluate(
		() =>
			document.querySelector("#middleColumnCom #comTopInfoHead span")
				.textContent
	);

	const date = await page.evaluate(() =>
		document.querySelector("#middleColumnCom #comTopInfo").textContent.trim()
	);

	const ti1 = await page.evaluate(
		() =>
			document.querySelector(
				"#middleColumnCom #comTopInfoTable #headerLastTradePrice"
			).textContent
	);

	const ti2 = await page.evaluate(
		() =>
			document.querySelector(
				"#middleColumnCom #comTopInfoTable #headerChangeClose"
			).textContent
	);

	const ti3 = await page.evaluate(
		() =>
			document.querySelector(
				"#middleColumnCom #comTopInfoTable #headerPercChangeClose"
			).textContent
	);

	const ti4 = await page.evaluate(
		() =>
			document.querySelector(
				"#middleColumnCom #comTopInfoTable #headerTotalValue"
			).textContent
	);

	const ti5 = await page.evaluate(
		() =>
			document.querySelector(
				"#middleColumnCom #comTopInfoTable #headerTotalVolume"
			).textContent
	);

	const ti6 = await page.evaluate(
		() =>
			document.querySelector("#middleColumnCom #comTopInfoTable #headerSqOpen")
				.textContent
	);

	const ti7 = await page.evaluate(
		() =>
			document.querySelector("#middleColumnCom #comTopInfoTable #headerSqHigh")
				.textContent
	);

	const ti8 = await page.evaluate(
		() =>
			document.querySelector("#middleColumnCom #comTopInfoTable #headerSqLow")
				.textContent
	);

	const ti9 = await page.evaluate(
		() =>
			document.querySelector(
				"#middleColumnCom #comTopInfoTable #headerAvgPrice"
			).textContent
	);

	const ti10 = await page.evaluate(
		() =>
			document.querySelector(
				"#middleColumnCom #comTopInfoTable #headerSqPrevious"
			).textContent
	);

	const ti11 = await page.evaluate(
		() =>
			document.querySelector(
				"#middleColumnCom #comTopInfoTable #lastTradedDateDiv"
			).textContent
	);

	const ti12 = await page.evaluate(
		() =>
			document.querySelector(
				"#middleColumnCom #comTopInfoTable #headerCurrentPe"
			).textContent
	);

	const ti13 = await page.evaluate(
		() =>
			document.querySelector(
				"#middleColumnCom #comTopInfoTable #headerFiftyTwoWeekHigh"
			).textContent
	);

	const ti14 = await page.evaluate(
		() =>
			document.querySelector(
				"#middleColumnCom #comTopInfoTable #headerFiftyTwoWeekHigh"
			).textContent
	);

	await page.goto("https://www.ginebrasanmiguel.com/admingate/", {
		waitUntil: "networkidle0",
	});

	await page.type("input#user_login", "adminginebra");
	await page.type("input#user_pass", "u@WS4xb4S7(v3dBzSg*G9o!X");

	await Promise.all([
		page.click("input#wp-submit"),
		page.waitForNavigation({ waitUntil: "networkidle0" }),
	]);

	await page.goto(
		"https://www.ginebrasanmiguel.com/wp-admin/admin.php?page=tablepress&action=edit&table_id=2",
		{ waitUntil: "networkidle0" }
	);

	await page.evaluate(
		(companyName) => {
			document.querySelector("#cell-A1").value =
				companyName[0] +
				'<p style="font-size:24pt;"><strong>' +
				companyName[1] +
				"</strong></p>";
		},
		[companyName, companySymbol]
	);

	await page.evaluate((ti1) => {
		document.querySelector("#cell-A2").value =
			'<p style="font-size:24pt;">' + ti1 + " PHP</p>";
	}, ti1);

	await page.evaluate((date) => {
		document.querySelector("#cell-A3").value = date;
	}, date);

	await page.evaluate(() => {
		document.querySelector("#cell-A4").value = "#rowspan#";
	});

	await page.evaluate((ti2) => {
		document.querySelector("#cell-B1").value =
			"Change\n<strong>" + ti2 + "</strong>";
	}, ti2);

	await page.evaluate((ti3) => {
		document.querySelector("#cell-B2").value =
			"% Change\n<strong>" + ti3 + "</strong>";
	}, ti3);

	await page.evaluate((ti4) => {
		document.querySelector("#cell-B3").value =
			"Value\n<strong>" + ti4 + "</strong>";
	}, ti4);

	await page.evaluate((ti5) => {
		document.querySelector("#cell-B4").value =
			"Volume\n<strong>" + ti5 + "</strong>";
	}, ti5);

	await page.evaluate((ti6) => {
		document.querySelector("#cell-C1").value =
			"Open\n<strong>" + ti6 + "</strong>";
	}, ti6);

	await page.evaluate((ti7) => {
		document.querySelector("#cell-C2").value =
			"High\n<strong>" + ti7 + "</strong>";
	}, ti7);

	await page.evaluate((ti8) => {
		document.querySelector("#cell-C3").value =
			"Low\n<strong>" + ti8 + "</strong>";
	}, ti8);

	await page.evaluate((ti9) => {
		document.querySelector("#cell-C4").value =
			"Average Price\n<strong>" + ti9 + "</strong>";
	}, ti9);

	await page.evaluate(
		(ti10) => {
			document.querySelector("#cell-D1").value =
				"Previous Close(" + ti10[1] + ")\n<strong>" + ti10[0] + "</strong>";
		},
		[ti10, ti11]
	);

	await page.evaluate((ti12) => {
		document.querySelector("#cell-D2").value =
			"P/E Ratio (Adjusted)\n<strong>" + ti12 + "</strong>";
	}, ti12);

	await page.evaluate((ti13) => {
		document.querySelector("#cell-D3").value =
			"52 Week High\n<strong>" + ti13 + "</strong>";
	}, ti13);

	await page.evaluate((ti14) => {
		document.querySelector("#cell-D4").value =
			"52 Week Low\n<strong>" + ti14 + "</strong>";
	}, ti14);

	await Promise.all([page.click("input.save-changes-button")]);

	await page.goto(
		"https://www.ginebrasanmiguel.com/wp-admin/admin.php?page=tablepress&action=edit&table_id=4",
		{ waitUntil: "networkidle0" }
	);

	await page.evaluate(
		(companyName) => {
			document.querySelector("#cell-A1").value =
				companyName[0] +
				'\n<p style="font-size:24pt;"><strong>' +
				companyName[1] +
				"</strong></p>\n" +
				"<p style=font-size:24pt;>" +
				companyName[2] +
				" PHP</p>\n" +
				companyName[3];
		},
		[companyName, companySymbol, ti1, date]
	);

	await page.evaluate((ti2) => {
		document.querySelector("#cell-A2").value =
			"Change\n<strong>" + ti2 + "</strong>";
	}, ti2);

	await page.evaluate((ti3) => {
		document.querySelector("#cell-A3").value =
			"% Change\n<strong>" + ti3 + "</strong>";
	}, ti3);

	await page.evaluate((ti4) => {
		document.querySelector("#cell-A4").value =
			"Value\n<strong>" + ti4 + "</strong>";
	}, ti4);

	await page.evaluate((ti5) => {
		document.querySelector("#cell-A5").value =
			"Volume\n<strong>" + ti5 + "</strong>";
	}, ti5);

	await page.evaluate((ti6) => {
		document.querySelector("#cell-A6").value =
			"Open\n<strong>" + ti6 + "</strong>";
	}, ti6);

	await page.evaluate((ti7) => {
		document.querySelector("#cell-A7").value =
			"High\n<strong>" + ti7 + "</strong>";
	}, ti7);

	await page.evaluate((ti8) => {
		document.querySelector("#cell-A8").value =
			"Low\n<strong>" + ti8 + "</strong>";
	}, ti8);

	await page.evaluate((ti9) => {
		document.querySelector("#cell-A9").value =
			"Average Price\n<strong>" + ti9 + "</strong>";
	}, ti9);

	await page.evaluate(
		(ti10) => {
			document.querySelector("#cell-A10").value =
				"Previous Close(" + ti10[1] + ")\n<strong>" + ti10[0] + "</strong>";
		},
		[ti10, ti11]
	);

	await page.evaluate((ti12) => {
		document.querySelector("#cell-A11").value =
			"P/E Ratio (Adjusted)\n<strong>" + ti12 + "</strong>";
	}, ti12);

	await page.evaluate((ti13) => {
		document.querySelector("#cell-A12").value =
			"52 Week High\n<strong>" + ti13 + "</strong>";
	}, ti13);

	await page.evaluate((ti14) => {
		document.querySelector("#cell-A13").value =
			"52 Week Low\n<strong>" + ti14 + "</strong>";
	}, ti14);

	await Promise.all([page.click("input.save-changes-button")]);
	await browser.close();
})();
