import * as Constants from "./constants.js";
import { searchTerms, fbFilterParameters, dateRange } from "./globals.js";
import ObjectsToCSV from "objects-to-csv";

const infiniteScroll = async (page, mode) => {
	let currentHeight, previousHeight, articleSelector;
	do {
		previousHeight = await page.evaluate("document.body.scrollHeight");
		await page.evaluate("window.scrollTo(0, document.body.scrollHeight)");
		await page.waitForTimeout(2500);
		currentHeight = await page.evaluate("document.body.scrollHeight");

		articleSelector =
			mode === "fb"
				? Constants.FB_PARENT_SELECTOR
				: Constants.TW_PARENT_SELECTOR;
		const articles = await page.evaluate(
			(articleSelector) =>
				`Array.from(document.querySelectorAll(${articleSelector}))`,
			articleSelector
		);

		if (articles.length > 50) break;
	} while (previousHeight !== currentHeight);
	return page;
};

const getPosts = async (page, mode) => {
	const scrolledPage = await infiniteScroll(page, mode);

	const scrapedItems = await scrolledPage.evaluate(
		(Constants) =>
			Array.from(
				document.querySelectorAll(Constants.FB_PARENT_SELECTOR),
				(post) => ({
					postDate: `${
						post.querySelector(Constants.FB_DATE_SELECTOR)?.firstChild
							.textContent
					}, 2021`,
					postLink: post.querySelector(Constants.FB_LINK_SELECTOR)?.href,
				})
			),
		Constants
	);
	const filteredItems = scrapedItems.filter(
		(item) =>
			item.postDate &&
			(isNaN(new Date(item.postDate).getTime()) ||
				new Date(item.postDate).getTime() - dateRange.end_date.getTime() > 0)
	);
	return filteredItems;
};

const getTweets = async (page, mode) => {
	const scrolledPage = await infiniteScroll(page, mode);

	const scrapedItems = await scrolledPage.evaluate(
		(Constants) =>
			Array.from(
				document.querySelectorAll(Constants.TW_PARENT_SELECTOR),
				(tweet) => ({
					AuthorName: tweet.querySelector(Constants.TW_AUTHOR_SELECTOR)
						?.textContent,
					PostLink: tweet.querySelector(Constants.TW_LINK_SELECTOR)?.href,
					PostContent: tweet.querySelector(Constants.TW_CONTENT_SELECTOR)
						?.textContent,
					DatePosted: new Date(
						tweet
							.querySelector(Constants.TW_DATE_SELECTOR)
							?.getAttribute("datetime")
					).toString(),
					Replies: tweet.querySelector(Constants.TW_COMMENTS_SELECTOR)
						?.textContent,
					Retweet: tweet.querySelector(Constants.TW_SHARES_SELECTOR)
						?.textContent,
				})
			),
		Constants
	);

	const filteredItems = scrapedItems.filter(
		(item) =>
			item.DatePosted &&
			(isNaN(new Date(item.DatePosted).getTime()) ||
				new Date(item.DatePosted).getTime() - dateRange.end_date.getTime() > 0)
	);

	return filteredItems;
};

const save = async (obj, topic, mode) => {
	const csv = new ObjectsToCSV(obj);
	const months = [
		"January",
		"February",
		"March",
		"April",
		"May",
		"June",
		"July",
		"August",
		"September",
		"October",
		"November",
		"December",
	];
	const date = dateRange.start_date;
	const year = date.getFullYear();
	const day = date.getDate();
	const month = months[date.getMonth()];
	await csv.toDisk(
		`./reports/${
			mode === "fb" ? "[ Facebook" : "[ Twitter"
		} (${topic.toUpperCase()}) ] ${month} ${day}, ${year} Report.csv`,
		{ append: true, bom: true }
	);
};

const getPostContent = async (page, posts) => {
	const postContent = [];

	for (let i = 0; i < posts.length; i++) {
		console.log(`Going to post#${i + 1}`);

		const { postLink, postDate } = { ...posts[i] };
		await page.goto(postLink, {
			waitUntil: "networkidle0",
		});

		const postHandle = await page.$('div[role*="article"].lzcic4wl');

		const author = await page.evaluate(
			(post) => post?.querySelector("h2")?.textContent,
			postHandle
		);
		const content = await page.evaluate(
			(post) =>
				post?.querySelector('div[data-ad-preview*="message"] span')
					?.textContent,
			postHandle
		);

		// const likes = await page.evaluate(
		// 	(post) => post?.querySelector("span.pcp91wgn")?.textContent,
		// 	postHandle
		// );

		const comments = await page.evaluate(
			(post) =>
				post?.querySelector("div.gtad4xkn:first-child span")?.textContent,
			postHandle
		);
		const shares = await page.evaluate(
			(post) =>
				post?.querySelector("div.gtad4xkn:last-child span")?.textContent,
			postHandle
		);
		const postObject = {
			author: author,
			link: postLink,
			content: content,
			date: postDate,
			comments: comments,
			shares: shares,
		};
		postContent.push(postObject);
	}
	return postContent;
};

const search = async (page, mode) => {
	const searchTopics = Object.keys(searchTerms);
	const fullSearch = [];

	for (let i = 0; i < searchTopics.length; i++) {
		const searchTopic = searchTopics[i];
		const keywordsArray = searchTerms[searchTopic];
		const searchResults = [];

		for (let j = 0; j < keywordsArray.length; j++) {
			let searchResult;

			if (mode === "fb") {
				searchResult = await searchFB(page, keywordsArray[j], mode);
			} else if (mode === "tw") {
				searchResult = await searchTw(page, keywordsArray[j], mode);
			}
			searchResults.push(searchResult);
		}

		const flattenArray = searchResults.flat();
		await save(flattenArray, searchTopic, mode);
		fullSearch.push(searchResults.flat());
	}

	return fullSearch;
};

const searchFB = async (page, keyword, mode) => {
	const searchResult = [];

	for (let i = 0; i < fbFilterParameters.length; i++) {
		const searchURL = `https://www.facebook.com/search/posts?q=${keyword}${fbFilterParameters[i]}`;
		await page.goto(searchURL, {
			waitUntil: "networkidle0",
		});
		console.log(`Searching for keyword: ${keyword} [${i + 1}]`);

		const posts = await getPosts(page, mode);
		console.log(`Total number of posts found: ${posts.length}`);

		const scrapeResult = await getPostContent(page, posts);
		searchResult.push(...scrapeResult);
	}
	return searchResult;
};

const searchTw = async (page, keyword, mode) => {
	const searchResult = [];
	const searchURL = `https://twitter.com/search?q=${keyword}&src=typed_query&lf=on&f=live`;
	await page.goto(searchURL, { waitUntil: "networkidle2" });
	console.log("Looking for keyword: " + keyword);

	const tweets = await getTweets(page, mode);
	console.log(`Total number of posts found: ${tweets.length}`);

	searchResult.push(...tweets);

	return searchResult;
};

export { search, getPosts };
