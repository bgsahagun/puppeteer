const WAIT_UNTIL_NETWORK_IDLE = "networkidle0";

const FB_PARENT_SELECTOR = `div[role*="article"].sjgh65i0`;
const FB_DATE_SELECTOR = "span.a3bd9o3v.m9osqain";
const FB_LINK_SELECTOR = "div.jb3vyjys > a.oajrlxb2";
const FB_AUTHOR_SELECTOR = "h2";
const FB_CONTENT_SELECTOR = `div[data-ad-preview*="message"] span`;
const FB_COMMENTS_SELECTOR = "div.gtad4xkn:first-child span";
const FB_SHARES_SELECTOR = "div.gtad4xkn:last-child span";

const TW_PARENT_SELECTOR = `article[role*="article"] > div > div > div > div[data-testid="tweet"] > div:last-child`;
const TW_DATE_SELECTOR =
	"div:first-child > div > div > div > div > div > div > a > time";
const TW_LINK_SELECTOR =
	"div:first-child > div > div > div > div > div > div > a";
const TW_AUTHOR_SELECTOR =
	"a[role*=link].css-4rbku5.css-18t94o4.css-1dbjc4n.r-1loqt21.r-1wbh5a2.r-dnmrzs.r-1ny4l3l > div > div > div > span > span";
const TW_CONTENT_SELECTOR = `div.css-901oao.r-18jsvk2.r-1qd0xha.r-a023e6.r-16dba41.r-rjixqe.r-bcqeeo.r-bnwqim.r-qvutc0`;
const TW_COMMENTS_SELECTOR = 'div[role*="group"] > div:first-child';
const TW_SHARES_SELECTOR = 'div[role*="group"] > div:nth-child(2)';

export {
	WAIT_UNTIL_NETWORK_IDLE,
	FB_PARENT_SELECTOR,
	FB_DATE_SELECTOR,
	FB_LINK_SELECTOR,
	FB_AUTHOR_SELECTOR,
	FB_CONTENT_SELECTOR,
	FB_COMMENTS_SELECTOR,
	FB_SHARES_SELECTOR,
	TW_PARENT_SELECTOR,
	TW_DATE_SELECTOR,
	TW_LINK_SELECTOR,
	TW_AUTHOR_SELECTOR,
	TW_CONTENT_SELECTOR,
	TW_COMMENTS_SELECTOR,
	TW_SHARES_SELECTOR,
};
