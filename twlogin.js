import puppeteer from "puppeteer";
import fs from "fs";
import config from "./config.json";

(async () => {
	const browser = await puppeteer.launch();
	const page = await browser.newPage();

	await page.goto("https://twitter.com/login", { waitUntil: "networkidle2" });

	await page.type(
		'form input[name="session[username_or_email]"]',
		config.twitter.username,
		{ delay: 30 }
	);
	await page.type(
		'form input[name="session[password]"]',
		config.twitter.password,
		{ delay: 30 }
	);

	await page.click("form div[role *= button]");
	await page.waitForNavigation({ waitUntil: "networkidle2" });

	try {
		await page.waitForSelector(`a[href*="${config.twitter.handler}"]`);
		console.log("Successfully logged in");
	} catch (error) {
		console.log("Failed to login");
		process.exit(0);
	}

	let currentCookies = await page.cookies();

	fs.writeFileSync("./twcookies.json", JSON.stringify(currentCookies));

	await browser.close();
})();
